/*
let rango = document.getElementById("interes_mensual");


rango.addEventListener("click",tomar_valor);
function tomar_valor(){
 let  valor = document.getElementById("cant_ano");
    valor.textContent=rango;
}

*/
/*
//funcion que valida  monto de prestamo
function ValidaMP(cantidadMP){	
	while((cantidadMP=="") || (cantidadMP==null) || (isNaN(cantidadMP))){
    alert("Informacion no valida...El dato -MONTO DEL PRESTAMO- debe ser numerico");
    document.getElementById("validationServer01").focus();  
    break; 
    }
    return cantidadMP;
}

//funcion que valida  interes mensual
function ValidaI(cantidadI){
	while((cantidadI=="") || (cantidadI==null) || (isNaN(cantidadI))){
    alert("Informacion no valida...El dato -INTERES MENSUAL- debe ser numerico");
    document.getElementById("interes_mensual").focus();  
    break;   
    }
    return cantidadI;
}

//funcion que valida  plazo mensual
function ValidaPM(cantidadPM){
	while((cantidadPM=="") || (cantidadPM==null) || (isNaN(cantidadPM))){
        alert("Informacion no valida...El dato -PLAZO MENSUAL- se debe tomar como numero entero");
        document.getElementById("validationServer02").focus();  
        break; 
    }
    return cantidadPM;
}
*/

let rango = document.getElementById("validationServer02");
  rango.addEventListener("change",pasar_dato);
  function pasar_dato(){
    let ano = document.getElementById("cant_ano");
    ano.textContent=rango.value;
    console.log(rango)
  }




//let limpiar = document.getElementById("limpiar").setAttribute("hidden", "hidden");

let boton = document.getElementById("btn-cal_prest").removeAttribute("hidden", "hidden");

function cargar_datos() {
  
  let boton = document.getElementById("btn-cal_prest").setAttribute("hidden", "hidden");

let limpiar= document.createElement("button")
let a_limpiar = document.getElementById("a_limpiar");
    a_limpiar.appendChild(limpiar);
  
    limpiar.setAttribute("id", "limpiar");
    limpiar.setAttribute("class", "btn btn-primary");
    limpiar.setAttribute("type", "button");
    limpiar.setAttribute("onclick", "location.reload()");
    limpiar.innerText="Limpiar";

  



  let monto_prestamo = document.getElementById("validationServer01").value;
    
    let plazo_mensual = document.getElementById("validationServer02").value;
    
    let interes = document.getElementById("interes_mensual").value/100;
    
    console.log(monto_prestamo);
    console.log(plazo_mensual);
    console.log(interes);
    
    let numerador = interes*monto_prestamo;

    let denominador = (1 - 1/Math.pow((1+interes),plazo_mensual));
    
    let cuota_mensual = 0;
    
    let saldo = monto_prestamo;
    
    let pago_capital = 0
    
    let int_m=0;

    //document.getElementById("btn-msj").innerHTML="Pago Mensual: B/. "+cuota_mensual.;
    
  
    //Aqui inicia la creacion de la tabla
      
    // Creamos el elemento "table" y el encabezado "tbody" 
    let tabla   = document.createElement("table");
        tabla.classList.add("table-striped")
    let tblBody = document.createElement("tbody");

    /*
    // Iniciamos la primera fila
    let fila = document.createElement("tr");
    */
    
    // Creamos las columnas
    for (let i = -1; i <= plazo_mensual; i++) {
    
      // Creamos las filas de la tabla

        if(i<=0){

        cuota_mensual=0;
        
        pago_capital=0;
        
        int_m=0;
        
      }else{
    
        cuota_mensual = (numerador/denominador).toFixed(2);
        //cuota_mensual.toLocaleString("en-US");
    
        saldo = (saldo - pago_capital).toFixed(2);
    
        int_m = (saldo*interes).toFixed(2);  
    
        pago_capital= (cuota_mensual - int_m).toFixed(2);
        }
      
      let fila = document.createElement("tr");
 
      for (let j = 0; j <= 5; j++) {    

        if(i<0){
            
                  // Creamos los encabezados
                  let celda = document.createElement("th");
          
                  if(i<0 && j==0){
                  let textoCelda = document.createTextNode("Período");
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }

                  if(i<0 && j==1){
                  let textoCelda = document.createTextNode("Saldo (B/.)");
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }
          
                  if(i<0 && j==2){
                  let textoCelda = document.createTextNode("Cuota Mensual (B/.)");
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }
          
                  if(i<0 && j==3){
                  let textoCelda = document.createTextNode("Interes "+interes+" % => (B/.)");
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }

                  if(i<0 && j==4){
                  let textoCelda = document.createTextNode("Abono Capital (B/.)");
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }
                  
                  if(i<0 && j==5){
                  let textoCelda = document.createTextNode("Nuevo Saldo (B/.)");
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }

            /*
            celda.appendChild(textoCelda);
            fila.appendChild(celda);
             */
        } else{
            // Creamos las filas y columnas con contenido
            
            

                  if(j==0 && i>=0){
                  let celda = document.createElement("th");
                  celda.classList.add("periodo");
                  //PERIODO
                  let textoCelda = document.createTextNode(i);
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }

                  if(j==1 && i>=0){
                  let celda = document.createElement("td");
                  //CUOTA
                  let textoCelda = document.createTextNode(saldo);
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }
            
                  if(j==2 && i>=0){          
                  let celda = document.createElement("td");
                  //INTERES
                  let textoCelda = document.createTextNode(cuota_mensual);
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }
           
                  if(j==3 && i>=0){
                  let celda = document.createElement("td");
                  //ABONO CAPITAL
                  let textoCelda = document.createTextNode(int_m);
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);                
                  }
               
                  if(j==4 && i>=0){
                  let celda = document.createElement("td");
                  //SALDO
                  let textoCelda = document.createTextNode(pago_capital);
                  celda.appendChild(textoCelda);
                  fila.appendChild(celda);
                  }

                  if(j==5 && i>=0){
                    let nuevo_saldo = (saldo-pago_capital).toFixed(2);
                    let celda = document.createElement("td");
                    //SALDO
                    let textoCelda = document.createTextNode(nuevo_saldo);
                    celda.appendChild(textoCelda);
                    fila.appendChild(celda);
                    }

        }
      }
      
    // agrega la fila al final de la tabla (al final del elemento tblbody)
    tblBody.appendChild(fila);
  }
 
  // posiciona el <tbody> debajo del elemento <table>
  tabla.appendChild(tblBody);
  // appends <table> into <tabla_resultados>
  tabla_calculos.appendChild(tabla);
  // modifica el atributo "border" de la tabla y lo fija a "1";
  tabla.setAttribute("border", "1");
  tabla.setAttribute("width", "100%");
  }

document.getElementById("btn-cal_prest").addEventListener("click", cargar_datos);