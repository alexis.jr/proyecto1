//Cambiamos el color de fondo
function random(numero){
    return Math.floor(Math.random()*numero);
}
//aqui termina


//ocultar link de "ver resultados"
function ocultar(){
    document.getElementById("ce").setAttribute("hidden","hidden");
}


//Aqui empieza
function cambiar_fondo(){
    let color = "rgb("+random(255)+","+random(255)+","+random(255)+",0.5)"
    return color
  }
//aqui termina



// Aqui controlamos la creacion de opciones cuando se crea la encuesta
let agregar_opcion = document.getElementById("agregar_opcion");
agregar_opcion.addEventListener("click",function(){
   
    let padre_opciones = document.getElementById("opciones");
    let hijo_opcion = document.createElement("input");
    padre_opciones.appendChild(hijo_opcion);    
    let id_elem = document.getElementsByName("opcion[]").length;
    hijo_opcion.setAttribute("type","text");
    hijo_opcion.setAttribute("class","form-control");
    hijo_opcion.setAttribute("name","opcion[]");
   
    hijo_opcion.setAttribute("id",id_elem);
  
    hijo_opcion.setAttribute("placeholder","Nueva opción");
    document.getElementById("crear_e").removeAttribute("hidden","hidden");
    hijo_opcion.focus(); 
    document.getElementById("btn-eliminar").removeAttribute("hidden","hidden");
})
//////// hasta aqui



//aqui eliminamos opciones
document.getElementById("btn-eliminar").addEventListener("click",eliminar);
function eliminar(){
    
  let item = document.getElementsByName("opcion[]").length;
  let items = document.getElementsByName("opcion[]");
  if(item>=1){
     document.getElementById("opciones").removeChild(document.getElementById(item-1)); 
  }
   else{
    document.getElementById("btn-eliminar").setAttribute("hidden","hidden");
    document.getElementById("crear_e").setAttribute("hidden","hidden"); 
   }
  console.log(item);
  console.log(items);

}


//Esto se dispara cuando se da click al boton de crear encuesta
function crear_encuesta(){
    document.getElementById("ce").removeAttribute("hidden","hidden");
    let valores_a = document.getElementsByClassName("form-control");
    let nom_encuesta = [];
    let val_encuesta = [];
    let col_encuesta = [];
    let matriz = {};
    for( let i=0; i<valores_a.length; i++){
        if(i==0){
            matriz[1]= valores_a[i].value;
        }
        else {
        nom_encuesta[i-1]=valores_a[i].value;
        val_encuesta[i-1]=random(255);
        col_encuesta[i-1]=cambiar_fondo();
        }
    }
    matriz[2]=nom_encuesta;
    matriz[3]=val_encuesta;
    matriz[4]=col_encuesta;
    console.log(matriz);
    
    document.getElementById("parte1").setAttribute("hidden","hidden");
    document.getElementById("parte3").setAttribute("hidden","hidden");    
    document.getElementById("parte2").removeAttribute("hidden","hidden");
    
    let grafico3 = document.getElementById("grafico").getContext("2d");
    let chart3 = new Chart(grafico3,{
    type:"bar",
    data:{
          labels:matriz[2], 
          datasets:[
              {
                  label:matriz[1],
                  backgroundColor:matriz[4],         
                  data:matriz[3],
              }
          ]
    },
    options:{
        scales:{
            yAxes:[{
                ticks:{
                    beginAtZero:true
                },
            }]
        },
    },


})





//aqui creamos la pantalla para votar
let tit = matriz[1];
    let nombres = matriz[2];
    let val = matriz[3];
    let vot = matriz[4];
    let tot = 0;
    document.getElementById("titu").innerHTML=tit;
    
    for(let k=0; k < nombres.length; k++){
     
        let input = document.createElement("input");
        let etq = document.createElement("label");
        let lin = document.createElement("hr");
        let sal = document.createElement("div");   
          
        tot = val[k] + tot;
      
        document.getElementById("resultados").appendChild(input);
        input.setAttribute("type","radio");
        input.setAttribute("name","opc");
        input.setAttribute("value","0");
        document.getElementById("resultados").appendChild(etq);
        etq.innerHTML="     "+nombres[k];
        document.getElementById("resultados").appendChild(lin);

        matriz[2][k] = matriz[2][k]+"("+val[k+1]+"-"+tot+")";
        
    }
   

    //Aqui cargamos la grafica si no se ha actualizado el select 
let grafico = document.getElementById("grafico").getContext("2d");
let chart = new Chart(grafico,{
    type:"bar",
    data:{
          labels:matriz[2], 
          datasets:[
              {
                  label:matriz[1]+"(TOTAL DE VOTOS: "+tot+")",
                  backgroundColor:matriz[4],         
                  data:matriz[3],
              }
          ]
    },
    options:{
        scales:{
            yAxes:[{
                ticks:{
                    beginAtZero:true
                },
            }]
        },
    },
    
})

   
    
//aqui termina


//Aqui se cargan las graficas despues de actualizar el select
let tipografico = document.getElementById("tipografica");
tipografico.addEventListener("change", function(){
const opcion = tipografico.value;
let padre=document.getElementById("graf");
let grafico2=document.getElementById("grafico");
padre.removeChild(grafico2);
let hijo = document.createElement("canvas");
padre.appendChild(hijo);
hijo.setAttribute("id","grafico");
grafico2 = document.getElementById("grafico").getContext("2d");
if(opcion==1){
let chart = new Chart(grafico2,{
    type:"bar",
    data:{
          labels: matriz[2],
          datasets:[
              {
                  label:matriz[1]+"(TOTAL DE VOTOS: "+tot+")",
                  backgroundColor:matriz[4],                      
                  data:matriz[3],
              }
          ]
    },
    options:{
        scales:{
            yAxes:[{
                ticks:{
                    beginAtZero:true
                },                
            }]
        },
    },
})
}
else if(opcion==2){
    let chart = new Chart(grafico2,{
        type:"pie",
        data:{
              labels:matriz[2],
              datasets:[
                  {
                      label:matriz[1]+"(TOTAL DE VOTOS: "+tot+")",
                      backgroundColor:matriz[4],
                      data:matriz[3],
                  }
              ]
        },
    })    
}
else{
let chart = new Chart(grafico2,{
    type:"line",
    data:{
          labels:matriz[2],
          datasets:[
              {
                  label:matriz[1]+"(TOTAL DE VOTOS: "+tot+")",
                  backgroundColor:cambiar_fondo(255),             
                  data:matriz[3],
              }
          ]
    },
    options:{
        scales:{
            yAxes:[{
                ticks:{
                    beginAtZero:true
                },
            }]
        },
    },
})
}
});
//aqui termina


//Aqui oculto una de las pantallas
let btn_ce = document.getElementById("ce");
let vista_1 = document.getElementById("parte1");
let vista_2 = document.getElementById("parte2");
btn_ce.addEventListener("click",ocultar_graf);
    function ocultar_graf(){
    btn_ce.setAttribute("hidden","hidden");
    vista_1.setAttribute("hidden","hidden");
    vista_2.removeAttribute("hidden","hidden");
 
   
     } 
//aqui termina





//aqui empieza la seccion para votar
document.getElementById("ir_voto").addEventListener("click",function(){

    document.getElementById("parte1").setAttribute("hidden","hidden");
    document.getElementById("parte2").setAttribute("hidden","hidden");
    document.getElementById("parte3").removeAttribute("hidden","hidden");
    
    document.getElementById("votar").addEventListener("click",votar)
    function votar(){
    
    let val = matriz[3];
    let vot = matriz[4];
    let rda_s= document.getElementsByName("opc");    
    for(x=0; x< rda_s.length; x++){
        if(rda_s[x].checked){
           //val[x]=val[x]+1;
           matriz[3][x]=matriz[3][x]+1;
           rda_s[x].checked = false;
        }
        console.log(val);
    }
    

    document.getElementById("parte3").setAttribute("hidden","hidden");    
    document.getElementById("parte2").removeAttribute("hidden","hidden");
    let grafico = document.getElementById("grafico").getContext("2d");
    let chart = new Chart(grafico,{
    type:"bar",
    data:{
          labels:matriz[2], 
          datasets:[
              {
                  label:matriz[1],
                  backgroundColor:matriz[4],         
                  data:matriz[3],
              }
          ]
    },
    options:{
        scales:{
            yAxes:[{
                ticks:{
                    beginAtZero:true
                },
            }]
        },
    },


})


}
    
 
    })

}

