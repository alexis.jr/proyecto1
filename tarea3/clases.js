class empresa{
	
	constructor(vendedor,cantidad){
	this.vendedor= vendedor;
	this.cantidad= cantidad;
	}
	
	saludar(){
	console.log(`hola soy  ${this.vendedor} y comprare con  ${this.cantidad} dolare`);
	}
	
	static definiction(){
		console.log('La empresa da comicion');
		document.write('La empresa da comicion');
	}
	
}

class desarrollador extends empresa{
	
	constructor(vendedor,cantidad,lugar){
		super(vendedor, cantidad);
		this.lugar=lugar;
	}
	
	saludodesarrollador(){
		super.saludar();
		console.log(`Vendo mercancias a buen precio en ${this.lugar}`);
	}
	
	saludar(){
		console.log('hola desde la sub-clase');
		document.write('hola desde la sub-clase');
	}
	
}

let Desarrollador = new desarrollador('ivan', 100, 'torrijos carter');

Desarrollador.saludodesarrollador();


